<?php namespace Programmerbingung\Account\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Moniker Back-end Controller
 */
class Moniker extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Programmerbingung.Account', 'account', 'moniker');
    }
}