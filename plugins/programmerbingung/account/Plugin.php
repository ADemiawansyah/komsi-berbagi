<?php namespace Programmerbingung\Account;

use System\Classes\PluginBase;

/**
 * account Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'account',
            'description' => 'No description provided yet...',
            'author'      => 'programmerbingung',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerNavigation()
    {
        return [
            'account' => [
                'label' => 'Account',
                'url' => \Backend::url('programmerbingung/account/user'),
                'icon' => 'icon-user',
                'permissions' => ['programmerbingung.account.*'],
                'order' => 500,

                'sideMenu' => [
                    'users' => [
                        'label' => 'Users',
                        'icon' => 'icon-user',
                        'url' => \Backend::url('programmerbingung/account/user'),
                        'permissions' => ['programmerbingung.account.access_users']
                    ],
                    'monikers' => [
                        'label' => 'Monikers',
                        'icon' => 'icon-user-secret',
                        'url' => \Backend::url('programmerbingung/account/moniker'),
                        'permissions' => ['programmerbingung.account.access_monikers']
                    ],
                ]
            ]
        ];
    }

}
