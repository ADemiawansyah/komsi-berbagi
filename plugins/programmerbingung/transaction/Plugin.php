<?php namespace Programmerbingung\Transaction;

use System\Classes\PluginBase;

/**
 * transaction Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'transaction',
            'description' => 'No description provided yet...',
            'author'      => 'programmerbingung',
            'icon'        => 'icon-leaf'
        ];
    }


    public function registerNavigation()
    {
        return [
            'transaction' => [
                'label'=> 'Transaction',
                'icon' => 'icon-money',
                'url' => \Backend::url('programmerbingung/transaction/transaction'),
                'permissions' => ['programmerbingung.transaction.*'],
                'order' => 600,

                'sideMenu' => [
                    'transactions' => [
                        'label'=> 'Transactions',
                        'icon' => 'icon-calculator',
                        'url' => \Backend::url('programmerbingung/transaction/transaction'),
                        'permissions' => ['programmerbingung.transaction.access_transaction'],
                    ],
                    'transaction_types' => [
                        'label'=> 'Transaction Types',
                        'icon' => 'icon-file',
                        'url' => \Backend::url('programmerbingung/transaction/transactiontype'),
                        'permissions' => ['programmerbingung.transaction.access_transaction_type'],
                    ]
                ]
            ]
        ];
    }
}
