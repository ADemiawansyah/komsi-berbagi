<?php namespace Programmerbingung\Transaction\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Transaction Type Back-end Controller
 */
class TransactionType extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Programmerbingung.Transaction', 'transaction', 'transactiontype');
    }
}