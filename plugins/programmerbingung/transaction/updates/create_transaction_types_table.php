<?php namespace Programmerbingung\Transaction\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTransactionTypesTable extends Migration
{

    public function up()
    {
        Schema::create('programmerbingung_transaction_transaction_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            # $table->integer('reference_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('programmerbingung_transaction_transaction_types');
    }

}
