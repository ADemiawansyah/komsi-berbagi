<?php namespace Programmerbingung\Transaction\Models;

use Model;

/**
 * Transaction Model
 */
class Transaction extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'programmerbingung_transaction_transactions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'balance_record' => 'Programmerbingung\Transaction\Models\BalanceRecord'
    ];
    public $hasMany = [];
    public $belongsTo = [
        'transaction_type' => 'Programmerbingung\Transaction\Models\TransactionType',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}