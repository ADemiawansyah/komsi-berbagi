<?php namespace Programmerbingung\Transaction\Models;

use Model;

/**
 * TransactionType Model
 */
class TransactionType extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'programmerbingung_transaction_transaction_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'transactions' => 'Programmerbingung\Transaction\Models\Transaction'
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}